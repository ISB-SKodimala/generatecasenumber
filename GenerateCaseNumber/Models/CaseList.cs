﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GenerateCaseNumber.Models
{
    public class CaseNumbersList
    {
        public int Id { get; set; }
        public string CaseNumber { get; set; }
        [Column("CENNumber")]
        public string CenNumber { get; set;}
      
    }
}
