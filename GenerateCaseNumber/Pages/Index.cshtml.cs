﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GenerateCaseNumber.Models;

namespace GenerateCaseNumber
{
    public class IndexModel : PageModel
    {
        private readonly GenerateCaseNumberContext _context;

        public IndexModel(GenerateCaseNumberContext context)
        {
            _context = context;
        }

        public IList<CaseNumbersList> CaseList { get;set; }

        public async Task OnGetAsync()
        {
            CaseList = await _context.CaseNumbersList.OrderByDescending(a => a.Id).ToListAsync();
        }
    }
}
