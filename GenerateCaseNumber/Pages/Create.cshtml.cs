﻿using System.Linq;
using GenerateCaseNumber.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Operations;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace GenerateCaseNumber.Pages
{
    public class CreateModel : PageModel
    {
        private readonly GenerateCaseNumber.Models.GenerateCaseNumberContext _context;
       
        public CreateModel(GenerateCaseNumber.Models.GenerateCaseNumberContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public CaseNumbersList CaseNumberList { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var caseList = await _context.CaseNumbersList.Where(a => a.CenNumber == CaseNumberList.CenNumber).ToListAsync();
            
            if (caseList.Count > 0)
            {
                TempData["msg"] = "CEN number already exists!";
                return RedirectToPage("./Index");
            }

            _context.CaseNumbersList.Add(CaseNumberList);
            await _context.SaveChangesAsync();
            var num = CaseNumberList.Id;
            string paddedstring = num.ToString("D3");

            CaseNumberList.CaseNumber = "18CR000" + paddedstring;
            await _context.SaveChangesAsync();
            return RedirectToPage("./Index");
        }
    }
}