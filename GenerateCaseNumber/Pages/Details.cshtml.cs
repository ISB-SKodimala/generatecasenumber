﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GenerateCaseNumber.Models;

namespace GenerateCaseNumber
{
    public class DetailsModel : PageModel
    {
        private readonly GenerateCaseNumber.Models.GenerateCaseNumberContext _context;

        public DetailsModel(GenerateCaseNumber.Models.GenerateCaseNumberContext context)
        {
            _context = context;
        }

        public CaseNumbersList CaseList { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CaseList = await _context.CaseNumbersList.FirstOrDefaultAsync(m => m.Id == id);

            if (CaseList == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
