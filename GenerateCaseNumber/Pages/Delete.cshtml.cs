﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GenerateCaseNumber.Models;

namespace GenerateCaseNumber
{
    public class DeleteModel : PageModel
    {
        private readonly GenerateCaseNumber.Models.GenerateCaseNumberContext _context;

        public DeleteModel(GenerateCaseNumber.Models.GenerateCaseNumberContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CaseNumbersList CaseList { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CaseList = await _context.CaseNumbersList.FirstOrDefaultAsync(m => m.Id == id);

            if (CaseList == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CaseList = await _context.CaseNumbersList.FindAsync(id);

            if (CaseList != null)
            {
                _context.CaseNumbersList.Remove(CaseList);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
