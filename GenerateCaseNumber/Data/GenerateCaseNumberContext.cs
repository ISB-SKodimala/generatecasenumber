﻿using  GenerateCaseNumber.Models;
using Microsoft.EntityFrameworkCore;

namespace GenerateCaseNumber.Models
{
    public class GenerateCaseNumberContext : DbContext
    {
        public GenerateCaseNumberContext (DbContextOptions<GenerateCaseNumberContext> options)
            : base(options)
        {
        }

        public DbSet<CaseNumbersList> CaseNumbersList { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CaseNumbersList>(entity => { entity.HasKey(e => e.Id); });
        }
    }
}
